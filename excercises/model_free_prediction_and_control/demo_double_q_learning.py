from adapsys.algorithms import double_q_learning
from adapsys.environments import MazeEnvironment

environment = MazeEnvironment()

q1, q2 = double_q_learning(environment, step_size=.1, epsilon=.7, n_episodes=1000, discount_factor=1.)
print(q1, q2)

q1, q2 = double_q_learning(environment, step_size=.1, epsilon=.7, n_episodes=1000, discount_factor=.9)
print(q1, q2)
