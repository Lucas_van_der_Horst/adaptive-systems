from adapsys.algorithms import sarsa
from adapsys.environments import MazeEnvironment

environment = MazeEnvironment()

q_table = sarsa(environment, step_size=.1, epsilon=.7, n_episodes=1000, discount_factor=1.)
print(q_table)

q_table = sarsa(environment, step_size=.1, epsilon=.7, n_episodes=1000, discount_factor=.9)
print(q_table)
