from adapsys.algorithms import first_visit_mc_prediction, value_iteration
from adapsys.policies import RandomPolicy
from adapsys.environments import MazeEnvironment

environment = MazeEnvironment()

policy = RandomPolicy(environment.action_space)
value_function = first_visit_mc_prediction(policy, environment, 1000)
environment.visualize_value_function(value_function)

policy = value_iteration(environment)
value_function = first_visit_mc_prediction(policy, environment, 100)
environment.visualize_value_function(value_function)
