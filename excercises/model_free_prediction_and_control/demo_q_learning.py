from adapsys.algorithms import q_learning
from adapsys.environments import MazeEnvironment

environment = MazeEnvironment()

policy = q_learning(environment, step_size=.1, epsilon=.7, n_episodes=1000, discount_factor=1.)
policy.epsilon = 0
environment.visualize_policy(policy)

policy = q_learning(environment, step_size=.1, epsilon=.7, n_episodes=1000, discount_factor=.9)
policy.epsilon = 0
environment.visualize_policy(policy)
