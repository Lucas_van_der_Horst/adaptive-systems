from adapsys.algorithms import on_policy_first_visit_mc_control
from adapsys.environments import MazeEnvironment

environment = MazeEnvironment()

policy = on_policy_first_visit_mc_control(environment, epsilon=.7, n_episodes=1000, discount_factor=1.)
policy.epsilon = 0.
environment.visualize_policy(policy)

policy = on_policy_first_visit_mc_control(environment, epsilon=.7, n_episodes=1000, discount_factor=.9)
policy.epsilon = 0.
environment.visualize_policy(policy)
