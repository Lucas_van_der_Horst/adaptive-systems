from adapsys.algorithms import td_learning, value_iteration
from adapsys.environments import MazeEnvironment

environment = MazeEnvironment()

policy = value_iteration(environment)
value_function = td_learning(policy, environment, .1, 1000, discount_factor=1.)
environment.visualize_value_function(value_function)

policy = value_iteration(environment)
value_function = td_learning(policy, environment, .1, 10000, discount_factor=.9)
environment.visualize_value_function(value_function)
