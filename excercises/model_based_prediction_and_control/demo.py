from adapsys.environments import MazeEnvironment
from adapsys.algorithms import value_iteration

# Create stochastic environment
env = MazeEnvironment(probabilities=(.7, .1, .1, .1))

# Create policy using value iteration
policy = value_iteration(env, discount_factor=1.)
env.visualize_value_function(policy.value_function)

# Test the new policy using simulation
print("Test the made policy with a simulation:")
obs, done = env.reset(), False
env.render()
while not done:
    action = policy(obs)
    obs, reward, done, info = env.step(action)
    env.render()


# Demo deterministic environment
env = MazeEnvironment()
print("Value iteration on deterministic environment")
policy = value_iteration(env, discount_factor=1.)
env.visualize_value_function(policy.value_function)
