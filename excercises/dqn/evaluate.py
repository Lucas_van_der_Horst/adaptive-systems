import torch
from train import dqn, env

dqn.load_state_dict(torch.load('very_good.pth'))
obs = env.reset()
while True:
    env.render()
    with torch.no_grad():
        action = torch.argmax(dqn(torch.from_numpy(obs))).item()
    #action = env.action_space.sample()
    obs, reward, done, info = env.step(action)
    if done:
        observation = env.reset()
env.close()
