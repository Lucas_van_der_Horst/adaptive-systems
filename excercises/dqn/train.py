from typing import *
import random
from collections import deque

import gym
import numpy as np
import torch
from torch import nn, optim
from torch.utils.tensorboard import SummaryWriter

Observation = np.ndarray
Action = int


class ExperiencePoint(NamedTuple):
    previous_observation: Observation
    action: Action
    reward: float
    terminal: bool
    next_observation: Observation


class ReplayMemory:
    def __init__(self, max_length: int):
        self.deque = deque(maxlen=max_length)

    def add(self, experience_point: ExperiencePoint) -> None:
        self.deque.append(experience_point)

    def __len__(self) -> int:
        return len(self.deque)

    def sample(self, batch_size: int) -> Tuple[List[Observation], List[Action], List[float], List[bool], List[Observation]]:
        batch = random.sample(self.deque, batch_size)
        return tuple(zip(*batch))


replay_memory = ReplayMemory(1000)
dqn = nn.Sequential(
    nn.Linear(8, 10),
    nn.ReLU(),
    nn.Linear(10, 6),
    nn.ReLU(),
    nn.Linear(6, 4),
)
optimizer = optim.Adam(dqn.parameters(), lr=1e-3)
loss_fn = nn.MSELoss(reduction='sum')
device = torch.device('cpu')
writer = SummaryWriter()
batch_size = 10
discount_factor = .95
epsilon = .4

env = gym.make("LunarLander-v2")


def train():
    episode = 0
    train_iteration = 0
    while True:
        prev_obs = env.reset()
        done = False
        total_rewards = 0.
        while not done:
            if random.random() > epsilon:
                action = env.action_space.sample()
            else:
                with torch.no_grad():
                    action = torch.argmax(dqn(torch.from_numpy(prev_obs))).item()

            next_obs, reward, done, info = env.step(action)
            replay_memory.add(
                ExperiencePoint(prev_obs, action, reward, done, next_obs)
            )
            total_rewards += reward
            prev_obs = next_obs

            if len(replay_memory) >= batch_size:
                prev_obs_batch, action_batch, reward_batch, terminal_batch, next_obs_batch = replay_memory.sample(batch_size)
                prev_obs_batch = torch.stack(list(map(torch.tensor, prev_obs_batch))).to(device)
                next_obs_batch = torch.stack(list(map(torch.tensor, next_obs_batch))).to(device)

                with torch.no_grad():
                    next_q_predictions = dqn(next_obs_batch)
                targets = torch.tensor([
                    reward if terminal else
                    reward + discount_factor * torch.max(next_q_pred).item()
                    for reward, terminal, next_q_pred in zip(reward_batch, terminal_batch, next_q_predictions)
                ])

                pred = dqn(prev_obs_batch)
                picked_pred = torch.stack([p[a] for p, a in zip(pred, action_batch)])

                loss = loss_fn(picked_pred.float(), targets.float())
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                writer.add_scalar('Loss/train', loss, train_iteration)
                train_iteration += 1
        writer.add_scalar('Total-reward', total_rewards, episode)
        episode += 1

        if episode % 100 == 0:
            torch.save(dqn.state_dict(), 'trained_dqn.pth')
            print(f"Saved model, episode: {episode}")

    env.close()


if __name__ == '__main__':
    train()
