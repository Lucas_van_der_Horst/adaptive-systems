from typing import Dict
from abc import ABCMeta, abstractmethod
from copy import deepcopy
import numpy as np
import gym


class State:
    def __init__(self, *args, **kwargs):
        self.prop = args

    def __hash__(self):
        return hash(self.prop)

    def __eq__(self, other):
        return hash(self) == hash(other)


class Action(metaclass=ABCMeta):
    def __init__(self, *args, **kwargs):
        self.prop = args

    def __hash__(self):
        return hash(self.prop)

    def __eq__(self, other):
        return hash(self) == hash(other)


class Policy(metaclass=ABCMeta):
    @abstractmethod
    def decide_action(self, state: State) -> Action:
        pass

    def __call__(self, *args, **kwargs):
        return self.decide_action(*args, **kwargs)


class ValueFunction(metaclass=ABCMeta):
    @abstractmethod
    def predict(self, state: State) -> float:
        pass

    def __call__(self, *args, **kwargs):
        return self.predict(*args, **kwargs)


class QFunction(metaclass=ABCMeta):
    @abstractmethod
    def predict(self, state: State, action: Action) -> float:
        pass

    def __call__(self, *args, **kwargs):
        return self.predict(*args, **kwargs)


class DictValueFunction(ValueFunction):
    def __init__(self):
        self.dict: Dict[State, float] = {}

    def set(self, state: State, value: float) -> None:
        self.dict[state] = value

    def predict(self, state: State) -> float:
        return self.dict.get(state, 0.)


class QTable(QFunction):
    def __init__(self, state_space: gym.spaces.Discrete, action_space: gym.spaces.Discrete):
        self.state_space = state_space
        self.action_space = action_space
        self.table = np.zeros((self.state_space.n, self.action_space.n))

    def set(self, state: State, action: Action, value: float) -> None:
        self.table[state][action] = value

    def predict(self, state: State, action: Action) -> float:
        return self.table[state][action]

    def get_n_actions(self) -> int:
        n_states, n_actions = self.table.shape
        return n_actions

    def __str__(self) -> str:
        output = ""
        for i, row in enumerate(self.table):
            output += f"{i:<2}: {' | '.join([f'{x:5.2f}' for x in row])}\n"
        return output

    def __add__(self, other) -> 'QTable':
        if isinstance(other, QTable):
            other: QTable
            assert self.state_space == other.state_space
            assert self.action_space == other.action_space
            new_q_table = deepcopy(self)
            new_q_table.table += other.table
            return new_q_table
        else:
            raise NotImplementedError
