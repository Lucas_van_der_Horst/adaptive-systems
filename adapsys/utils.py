from typing import List, NamedTuple, Sequence
from numbers import Real
from adapsys import Policy, State, Action
from adapsys.environments import DiscreteEnvironment


class SAR(NamedTuple):
    state: State
    action: Action
    reward: float


def rollout(environment: DiscreteEnvironment, policy: Policy, start_state: State) -> List[SAR]:
    episode_rollout = []
    prev_state = environment.set_state(start_state)
    done = False
    while not done:
        action = policy(prev_state)
        next_state, reward, done, info = environment.step(action)
        episode_rollout.append(SAR(prev_state, action, reward))
        prev_state = next_state
    return episode_rollout


def average(sequence: Sequence[Real]) -> float:
    return sum(sequence) / len(sequence)
