from typing import Optional, Tuple, Set, Sequence
from abc import ABCMeta, abstractmethod
import random
import gym
from adapsys import State, Action

Environment = gym.Env


class DiscreteState(State, int):
    def __init__(self, *args, **kwargs):
        super(DiscreteState, self).__init__(*args, **kwargs)


class DiscreteAction(Action, int):
    def __init__(self, *args, **kwargs):
        super(DiscreteAction, self).__init__(*args, **kwargs)


class DiscreteEnvironment(gym.Env, metaclass=ABCMeta):
    State = DiscreteState
    Action = DiscreteAction
    terminal_states = {}

    @abstractmethod
    def render(self, mode="human") -> None:
        pass

    @abstractmethod
    def set_state(self, state: Optional[State] = None) -> State:
        pass

    def reset(self) -> State:
        return self.set_state()

    @abstractmethod
    def step(self, action: Action) -> Tuple[State, float, bool, dict]:
        pass

    @abstractmethod
    def non_terminal_states(self) -> Set[State]:
        pass


class DiscreteStochasticEnvironment(DiscreteEnvironment, metaclass=ABCMeta):
    @abstractmethod
    def possible_steps(self, action: Action) -> Sequence[Tuple[State, float, float]]:
        pass

    def step(self, action: Action) -> Tuple[State, float, bool, dict]:
        assert action in self.action_space

        next_states, probabilities, rewards = zip(*self.possible_steps(action))
        (state, reward), = random.choices(list(zip(next_states, rewards)), probabilities)
        self.set_state(state)
        done = state in self.terminal_states
        info = {}

        return state, reward, done, info
