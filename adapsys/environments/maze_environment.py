from typing import Tuple, Set, Sequence, List, Optional
from dataclasses import dataclass
import gym
from adapsys.environments import DiscreteEnvironment, DiscreteStochasticEnvironment
from adapsys import ValueFunction, Policy


@dataclass(frozen=True)
class Coord:
    x: int
    y: int
    space_shape: Tuple[int, int]

    def to_state(self) -> 'MazeEnvironment.State':
        maze_width, maze_height = self.space_shape
        return DiscreteEnvironment.State(self.y * maze_width + self.x)

    @staticmethod
    def from_state(state: 'MazeEnvironment.State', space_shape: Tuple[int, int]) -> 'Coord':
        maze_width, maze_height = space_shape
        return Coord(state % maze_width, state // maze_width, space_shape)


class MazeEnvironment(DiscreteStochasticEnvironment):
    """The Magic Maze environment largely follows the OpenAI gym conventions."""

    def __init__(self, maze_rewards: Optional[Sequence[Sequence[float]]] = None,
                 start_state: Optional['MazeEnvironment.State'] = None,
                 probabilities: Tuple[float, float, float, float] = (1., 0., 0., 0.)):
        if maze_rewards is None:
            self.maze_rewards = [[-1., -1., -1., 40.],
                                 [-1., -1., -10, -10],
                                 [-1., -1., -1., -1.],
                                 [10., -2., -1., -1.]]
        else:
            self.maze_rewards = maze_rewards

        assert min(map(len, self.maze_rewards)) == max(map(len, self.maze_rewards))
        maze_width, maze_height = self.maze_shape = len(self.maze_rewards), len(self.maze_rewards[0])

        if start_state is None:
            self.start_state = Coord(2, 3, self.maze_shape).to_state()
        else:
            self.start_state = start_state

        self.current_state = start_state
        self.probabilities = probabilities

        self.action_space = gym.spaces.Discrete(4)
        self.observation_space = gym.spaces.Discrete(maze_width * maze_height)
        self.terminal_states = {Coord(3, 0, self.maze_shape).to_state(), Coord(0, 3, self.maze_shape).to_state()}

    def possible_steps(self, action: 'MazeEnvironment.Action') -> List[Tuple['MazeEnvironment.State', float, float]]:
        """
        Return all the possible resulting states of the given action (in the current state)
        with their probability and resulting reward.

        :param action: The action to evaluate.
        :return: A list with tuples containing:
            state: The resulting state.
            probability: The probability of getting in that state given the action.
            reward: The resulting reward.
        """
        assert action in self.action_space
        maze_height, maze_width = self.maze_shape
        agent_location = Coord.from_state(self.current_state, self.maze_shape)
        prev_x, prev_y = agent_location.x, agent_location.y
        dx, dy = [
            (0, -1),
            (0, 1),
            (-1, 0),
            (1, 0)
        ][action]

        possible_steps = []
        for probability in self.probabilities:
            # Create next state
            next_x, next_y = prev_x + dx, prev_y + dy
            if not(0 <= next_x < maze_width and 0 <= next_y < maze_height):
                next_x, next_y = prev_x, prev_y

            # Create reward
            reward = self.maze_rewards[next_y][next_x]

            # Append
            possible_steps.append((Coord(next_x, next_y, self.maze_shape).to_state(), probability, reward))

            # Rotate
            dx, dy = dy, -dx
        return possible_steps

    def non_terminal_states(self) -> Set['MazeEnvironment.State']:
        """
        Give the set of all states except for the terminal-states.

        :return: The set of all states except for the terminal-states.
        """
        states = set()
        maze_height, maze_width = self.maze_shape
        for y in range(maze_height):
            for x in range(maze_width):
                states.add(Coord(x, y, self.maze_shape).to_state())
        return states - self.terminal_states

    def set_state(self, state: Optional['MazeEnvironment.State'] = None) -> 'MazeEnvironment.State':
        """
        Resets the environment.

        :param state: Optional set the environment to the given state,
                      otherwise the state it has been initialized with will be used.
        :return: The state, this can be used as the first observation to kick off the simulation.
        """
        if state is None:
            self.current_state = self.start_state
        else:
            self.current_state = state
        return self.current_state

    def render(self, mode="human") -> None:
        """Visualize the environment on the state by printing the maze with the player location."""
        maze_height, maze_width = self.maze_shape
        for y in range(maze_height):
            for x in range(maze_width):
                print('웃' if Coord(x, y, self.maze_shape).to_state() == self.current_state else '.', end='')
            print()
        print()

    def visualize_value_function(self, value_function: ValueFunction):
        maze_height, maze_width = self.maze_shape
        for y in range(maze_height):
            # Print value function row
            print(' | '.join([f"{round(value_function.predict(Coord(x, y, self.maze_shape).to_state()), 1):<5}" for x in range(maze_width)]), '    ')
        print()

    def visualize_policy(self, policy: Policy) -> None:
        """Visualize the policy by printing the value for each state and the action for each state."""
        maze_height, maze_width = self.maze_shape
        for y in range(maze_height):
            # Print actions row
            for x in range(maze_width):
                if Coord(x, y, self.maze_shape).to_state() in self.terminal_states:
                    print('◯', end='')
                else:
                    print(['↑', '↓', '←', '→'][policy(Coord(x, y, self.maze_shape).to_state())], end='')
            print()
        print()
