import sys

if 'adapsys.types' not in sys.modules:
    from adapsys.types import \
        State, Action, Policy, ValueFunction, QFunction, DictValueFunction, QTable

if 'adapsys.utils' not in sys.modules:
    from adapsys.utils import SAR, rollout, average
