from typing import Optional
from copy import copy
from adapsys import State, Action, Policy, DictValueFunction
from adapsys.environments import DiscreteStochasticEnvironment


class ValueBasedPolicy(Policy):
    """
    A policy that chooses actions based on an internal value-function,
    naturally this means it also needs an internal model of the environment.
    There is also a method included that computes this value-function using value-iteration.
    """

    def __init__(self, environment: DiscreteStochasticEnvironment,
                 value_function: Optional[DictValueFunction] = None,
                 discount_factor: float = 1.):
        self.environment = copy(environment)
        if value_function is None:
            self.value_function = DictValueFunction()
        else:
            self.value_function = value_function
        self.discount_factor = discount_factor

    def actions_value(self, state: State, action: Action) -> float:
        """
        Compute the value of the given state looking at the next state.
        The next state is created from the given action, and evaluated using the internal value-function.

        :param state: The state to evaluate.
        :param action: The action used to create the next state.
        :return: Value calculated with the Bellman equation.
        """
        self.environment.set_state(state)
        return sum([
            # Bellman equation
            probability * (reward + self.discount_factor * self.value_function.predict(next_state))
            for next_state, probability, reward in self.environment.possible_steps(action)
        ])

    def decide_action(self, state: State) -> Action:
        """
        Choose an action based on the given state/observation
        aka the definition of a policy.

        :param state: The state/observation to base the decision on.
        :return: One of the actions available for the Magic Maze environment
        """
        return max(range(self.environment.action_space.n), key=lambda a: self.actions_value(state, a))
