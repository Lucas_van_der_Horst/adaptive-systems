from typing import Optional
import random
from adapsys import Policy, QTable
from adapsys.environments import DiscreteEnvironment


class QBasedPolicy(Policy):
    def __init__(self, q_table: QTable, epsilon: float = .0):
        self.q_table = q_table
        self.epsilon = epsilon

    def decide_action(self, state: DiscreteEnvironment.State, epsilon: Optional[float] = None) -> DiscreteEnvironment.Action:
        epsilon = self.epsilon if epsilon is None else epsilon
        n_actions = self.q_table.get_n_actions()
        optimal_action = max(range(n_actions), key=lambda i: self.q_table(state, i))
        probabilities = [
            1 - epsilon + epsilon / n_actions
            if i == optimal_action
            else epsilon / n_actions
            for i in range(n_actions)
        ]
        action, = random.choices(range(n_actions), probabilities)
        return action
