import gym
from adapsys import Action, State, Policy


class RandomPolicy(Policy):
    def __init__(self, action_space: gym.Space):
        self.action_space = action_space

    def decide_action(self, state: State) -> Action:
        return self.action_space.sample()
