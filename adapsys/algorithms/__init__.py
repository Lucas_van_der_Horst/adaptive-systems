import sys

if 'adapsys.algorithms.value_iteration' not in sys.modules:
    from adapsys.algorithms.value_iteration import value_iteration

if 'adapsys.algorithms.mc_policy_evaluation' not in sys.modules:
    from adapsys.algorithms.mc_policy_evaluation import first_visit_mc_prediction

if 'adapsys.algorithms.temporal_difference_learning' not in sys.modules:
    from adapsys.algorithms.temporal_difference_learning import td_learning

if 'adapsys.algorithms.on_policy_mc_control' not in sys.modules:
    from adapsys.algorithms.on_policy_mc_control import on_policy_first_visit_mc_control

if 'adapsys.algorithms.sarsa' not in sys.modules:
    from adapsys.algorithms.sarsa import sarsa

if 'adapsys.algorithms.q_learning' not in sys.modules:
    from adapsys.algorithms.q_learning import q_learning

if 'adapsys.algorithms.double_q_learning' not in sys.modules:
    from adapsys.algorithms.double_q_learning import double_q_learning
