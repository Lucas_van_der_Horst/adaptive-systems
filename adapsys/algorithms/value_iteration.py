from adapsys.environments import DiscreteStochasticEnvironment
from adapsys.policies import ValueBasedPolicy


def value_iteration(environment: DiscreteStochasticEnvironment, discount_factor: float = 1.,
                    threshold: float = .1) -> ValueBasedPolicy:
    """
    Compute the optimal policy by computing the optimal value-function
    from the given environment model using value iteration.

    :param environment: The environment model to "learn".
    :param discount_factor: Discount factor for the Bellman equation.
    :param threshold: Under what threshold of value-function change should the value iteration stop.
    :return: The created policy.
    """
    policy = ValueBasedPolicy(environment, discount_factor=discount_factor)
    delta_value_function = float('inf')

    # This is the literal implementation of the value iteration algorithm pseudocode
    while delta_value_function >= threshold:
        delta_value_function = 0
        for state in environment.non_terminal_states():
            previous_value = policy.value_function.predict(state)
            policy.value_function.set(state, policy.actions_value(state, policy(state)))
            new_value = policy.value_function.predict(state)
            delta_value_function = max(delta_value_function, abs(previous_value - new_value))

    return policy
