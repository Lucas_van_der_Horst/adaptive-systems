from itertools import cycle
from adapsys import QTable
from adapsys.environments import DiscreteEnvironment
from adapsys.policies import QBasedPolicy


def sarsa(env: DiscreteEnvironment, step_size: float, epsilon: float, n_episodes: int, discount_factor: float) -> QTable:
    q_table = QTable(env.observation_space, env.action_space)
    q_based_policy = QBasedPolicy(q_table, epsilon)
    start_state_iterator = cycle(env.non_terminal_states())
    for _ in range(n_episodes):
        state = env.set_state(next(start_state_iterator))
        action = q_based_policy(state)
        done = False
        while not done:
            next_state, reward, done, info = env.step(action)
            next_action = q_based_policy(next_state)
            q_table.set(
                state, action,
                q_table(state, action) + step_size * (reward + discount_factor * q_table(next_state, next_action) - q_table(state, action))
            )
            state, action = next_state, next_action
    return q_table
