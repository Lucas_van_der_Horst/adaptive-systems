from typing import Dict, List
from itertools import cycle
from adapsys import QTable, rollout, average
from adapsys.environments import DiscreteEnvironment
from adapsys.policies import QBasedPolicy


def on_policy_first_visit_mc_control(env: DiscreteEnvironment, n_episodes: int, epsilon: float, discount_factor: float = 1.) -> QBasedPolicy:
    q_based_policy = QBasedPolicy(QTable(env.observation_space, env.action_space), epsilon)
    returns: Dict[(env.State, env.Action), List[float]] = {}
    start_state_iterator = cycle(env.non_terminal_states())

    for _ in range(n_episodes):
        episode_rollout = rollout(env, q_based_policy, next(start_state_iterator))
        g = 0
        for t, (state, action, reward) in reversed(list(enumerate(episode_rollout))):
            g = discount_factor * g + reward
            if state not in returns:
                returns[(state, action)] = []
            returns[(state, action)].append(g)
            q_based_policy.q_table.set(state, action, average(returns[(state, action)]))
    return q_based_policy
