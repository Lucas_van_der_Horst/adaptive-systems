from itertools import cycle
from adapsys import Policy, DictValueFunction
from adapsys.environments import DiscreteEnvironment


def td_learning(policy: Policy, environment: DiscreteEnvironment, step_size: float, n_episodes: int,
                discount_factor: float = 1.) -> DictValueFunction:
    v = DictValueFunction()
    start_state_iterator = cycle(environment.non_terminal_states())
    for _ in range(n_episodes):
        prev_state = environment.set_state(next(start_state_iterator))
        done = False
        while not done:
            action = policy(prev_state)
            next_state, reward, done, info = environment.step(action)
            v.set(prev_state, v(prev_state) + step_size * (reward + discount_factor * v(next_state) - v(prev_state)))
            prev_state = next_state

    return v
