from typing import Dict, List
from itertools import cycle
from adapsys import Policy, DictValueFunction, rollout, average
from adapsys.environments import DiscreteEnvironment


def first_visit_mc_prediction(policy: Policy, environment: DiscreteEnvironment, n_episodes: int, discount_factor: float = 1.) -> DictValueFunction:
    value_function = DictValueFunction()
    returns: Dict[environment.State, List[float]] = {}
    start_state_iterator = cycle(environment.non_terminal_states())

    for _ in range(n_episodes):
        episode_rollout = rollout(environment, policy, next(start_state_iterator))
        g = 0.
        for t, (state, action, reward) in reversed(list(enumerate(episode_rollout))):
            g = discount_factor * g + reward
            if state not in [episode_rollout[i].state for i in range(t)]:
                if state not in returns:
                    returns[state] = []
                returns[state].append(g)
                value_function.set(state, average(returns[state]))

    return value_function
