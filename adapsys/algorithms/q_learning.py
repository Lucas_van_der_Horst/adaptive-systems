from itertools import cycle
from adapsys import QTable
from adapsys.environments import DiscreteEnvironment
from adapsys.policies import QBasedPolicy


def q_learning(env: DiscreteEnvironment, step_size: float, epsilon: float, n_episodes: int, discount_factor: float) -> QBasedPolicy:
    q_table = QTable(env.observation_space, env.action_space)
    q_based_policy = QBasedPolicy(q_table, epsilon)
    start_state_iterator = cycle(env.non_terminal_states())
    for _ in range(n_episodes):
        state = env.set_state(next(start_state_iterator))
        done = False
        while not done:
            action = q_based_policy(state)
            next_state, reward, done, info = env.step(action)
            q_table.set(
                state, action,
                q_table(state, action) + step_size * (reward + discount_factor * max(q_table.table[next_state]) - q_table(state, action))
            )
            state = next_state
    return q_based_policy
