from typing import Tuple
from itertools import cycle
import random
from adapsys import QTable
from adapsys.environments import DiscreteEnvironment
from adapsys.policies import QBasedPolicy


def double_q_learning(env: DiscreteEnvironment, step_size: float, epsilon: float, n_episodes: int, discount_factor: float) -> Tuple[QTable, QTable]:
    q1 = QTable(env.observation_space, env.action_space)
    q2 = QTable(env.observation_space, env.action_space)
    start_state_iterator = cycle(env.non_terminal_states())
    for _ in range(n_episodes):
        state = env.set_state(next(start_state_iterator))
        done = False
        while not done:
            action = QBasedPolicy(q1 + q2)(state)
            next_state, reward, done, info = env.step(action)
            if random.random() > .5:
                q1.set(
                    state, action,
                    q1(state, action) + step_size * (reward + discount_factor * q2(next_state, QBasedPolicy(q1, epsilon=0.)(next_state)) - q1(state, action))
                )
            else:
                q2.set(
                    state, action,
                    q2(state, action) + step_size * (reward + discount_factor * q1(next_state, QBasedPolicy(q2, epsilon=0.)(next_state)) - q2(state, action))
                )
            state = next_state
    return q1, q2
